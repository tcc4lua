all:test5_mod.so

CFLAGS= -O2 -Wall -g
PKG_CONFIG= libtcc lua5.1
EXTRA_CFLAGS= `pkg-config $(PKG_CONFIG) --cflags` -fPIC
LDLIBS= `pkg-config $(PKG_CONFIG) --libs` -lc
LDFLAGS= --as-needed --no-undefined -z defs

test5_mod.so:test5_mod.o
	$(LD) $(LDFLAGS) -shared -o $@ $< $(LDLIBS)

clean:
	rm -f test5_mod.o test5_mod.so

%.o: %.cpp
	g++ -o $@ -c $+ $(CFLAGS) $(EXTRA_CFLAGS)

%.o: %.c
	gcc -o $@ -c $+ $(CFLAGS) $(EXTRA_CFLAGS)

.PHONY: all clean


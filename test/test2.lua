print "luatcc working compilation test:"
print "--------------------------------"

local luatcc = require 'tcc'
local context = luatcc.new()

context:add_include_path('/usr/include/lua5.1')
context:define_symbol('STR','"Hello World!"')
context:compile([[
#include <lua.h>
int foo(lua_State* L)
{
	lua_pushstring(L, STR);
	return 1;
}
]])
context:undefine_symbol('STR')
context:relocate()
local foo = context:get_symbol("foo")
print(foo())

==============================================================================
 luatcc 1.0.0
==============================================================================
------------------------------------------------------------------------------
 About
------------------------------------------------------------------------------

This is the first release of luatcc, a Lua binding for libtcc library. You can
find documentation about luatcc at the following web address:

  http://luatcc.luaforge.net/

------------------------------------------------------------------------------
 Build instructions
------------------------------------------------------------------------------

To build luatcc edit config-gcc.mak and then run make in the top directory:

$ vi config-gcc.mak
$ make
$ make install

------------------------------------------------------------------------------
 TCC
------------------------------------------------------------------------------

To use luatcc you need TCC. It is available at:

  http://fabrice.bellard.free.fr/tcc/

Also, in the ext subdirectory of this package, you will find a patch named
tcc-tcc_compile_named_string.patch that can be applied to tcc to improve its
error messages when used with luatcc. This patch was written for TCC 0.9.23,
but can be easily adapted to other versions. If you apply this patch, comment
the last line of config-gcc.mak

------------------------------------------------------------------------------
 Testing the module
------------------------------------------------------------------------------

In the test subdirectory you will find some test programs to check that
everything is working properly. There is no automated test for the moment,
look at each testx.lua file to have an idea of what each test does.

------------------------------------------------------------------------------
 Documentation & Support
------------------------------------------------------------------------------

Documentation is available online at:

  http://luatcc.luaforge.net/

All support is done through the luatcc users mailing list:

  luatcc-users@lists.luaforge.net

To subscribe visit the following web page:

  http://lists.luaforge.net/mailman/listinfo/luatcc-users

------------------------------------------------------------------------------
 Credits & license
------------------------------------------------------------------------------
This module is written by Jérôme Vuarand. It is originally based on lua-tcc
module by Javier Guerra and has been extended to support a bigger part of
libtcc API and to work as a Lua module loader.

Luatcc is available under a MIT-style license. See LICENSE.txt.


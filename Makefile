# Installation directory
INSTALL_TOP_SHARE=/usr/share/lua/5.1
INSTALL_TOP_LIB=/usr/lib/lua/5.1

# Module name
MODULE=tcc
DLL=$(MODULE).so

# If your libtcc does contain tcc_compile_named_string comment next line.
CPPFLAGS+=

.PHONY:all
all:build

SRC=tcc4lua.c

CFLAGS= -O2 -Wall -g
EXTRA_CFLAGS= `pkg-config libtcc lua5.1 --cflags` -fPIC
EXTRA_CFLAGS+= -Dluaopen_module=luaopen_$(MODULE) -DMODULE_API=extern
EXTRA_CFLAGS+= -D'tcc_compile_named_string(tcc,code,chunkname)=tcc_compile_string(tcc,code)'
EXTRA_CFLAGS+= -I/usr/include/lua5.1
LDLIBS= `pkg-config libtcc lua5.1 --libs` -lc
LDFLAGS= --as-needed --no-undefined -z defs

%.o: %.cpp
	g++ -o $@ -c $+ $(CFLAGS) $(EXTRA_CFLAGS)

%.o: %.c
	gcc -o $@ -c $+ $(CFLAGS) $(EXTRA_CFLAGS)

.PHONY:build
build:$(DLL)

$(DLL):$(patsubst %.c,%.o,$(SRC))
	$(LD) $(LDFLAGS) -shared -o $@ $^ $(LDLIBS)

.PHONY:clean
clean:
	rm -f $(DLL) $(patsubst %.c,%.o,$(SRC))

DESTDIR =
LIB_DIR = /usr/lib/lua/5.1/
MODULE_DIR = /usr/share/lua/5.1/$(MODULE)

.PHONY:install
install:build
	mkdir -p $(DESTDIR)/$(LIB_DIR)
	cp $(DLL) $(DESTDIR)/$(LIB_DIR)
	mkdir -p $(DESTDIR)/$(MODULE_DIR)
	cp loader.lua $(DESTDIR)/$(MODULE_DIR)

.PHONY:uninstall
uninstall:
	rm -f $(DESTDIR)/$(DESTDIR_LIB)/$(DLL)
	rm -f $(DESTDIR)/$(MODULE_DIR)/loader.lua

/*
 * Copyright (C) 2009  Miriam Ruiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <lua.h>
#include <lauxlib.h>

#ifndef lua_boxpointer
#define lua_boxpointer(L,u) (*(void **)(lua_newuserdata(L, sizeof(void *))) = (u))
#define lua_unboxpointer(L,i) (*(void **)(lua_touserdata(L, i)))
#endif

typedef enum
{
	cb_test = 1,
	cb_total /* total number of allowed callback functions */
} cb_types;

static const char *const cb_names[] =
{
	"", /* empty on purpose */
	"test",
	NULL /* end of the list */
};

/* State Management */

typedef struct S_State
{
	unsigned int id;
	int cb_func[cb_total];
} State;

static unsigned int last_id = 0;

static State* push_state (lua_State *L, State *state)
{
	lua_boxpointer(L, state);
	lua_pushvalue(L, lua_upvalueindex(1));
	lua_setmetatable(L, -2);
	return state;
}

static State* check_state (lua_State *L, int index)
{
	luaL_checktype(L, index, LUA_TUSERDATA);
	lua_getmetatable(L, index);
	if( ! lua_equal(L, lua_upvalueindex(1), -1) ) /* die */
		luaL_typerror(L, index, "Invalid testcb object");
	lua_pop(L, 1);
	return (State*)lua_unboxpointer(L, index);
}

/* Auxiliary Functions */

static void StackDump (lua_State *L)
{
	int i;
	int top = lua_gettop(L);
	for (i = 1; i <= top; i++) /* repeat for each level */
	{
		int t = lua_type(L, i);
		switch (t)
		{

			case LUA_TSTRING: /* strings */
				printf("> '%s'", lua_tostring(L, i));
				break;

			case LUA_TBOOLEAN: /* booleans */
				printf("> %s", lua_toboolean(L, i) ? "true" : "false");
				break;

			case LUA_TNUMBER: /* numbers */
				printf("> %g", lua_tonumber(L, i));
				break;

			default: /* other values */
				printf("> %s", lua_typename(L, t));
				break;

		}
		printf("  "); /* put a separator */
	}
	printf("\n"); /* end the listing */
}

/* Callback Functions */

static int callback_register(lua_State * L)
{
	State* state = check_state(L, 1);
	if (!lua_isstring(L, 2) || !lua_isfunction(L, 3))
	{
		lua_pushnil(L);
		lua_pushstring(L, "Invalid arguments to callback.register.");
		return 2;
	}
	const char *s = lua_tostring(L, 2);
	int cb;
	for (cb = 0; cb < cb_total; cb++)
	{
		if (strcmp(cb_names[cb], s) == 0)
			break;
	}
	if (cb == cb_total)
	{
		lua_pushnil(L);
		lua_pushstring(L, "No such callback exists.");
		return 2;
	}
	if (state->cb_func[cb])
		luaL_unref(L, LUA_REGISTRYINDEX, state->cb_func[cb]);
	lua_pushvalue(L, 3);
	state->cb_func[cb] = luaL_ref(L, LUA_REGISTRYINDEX);
	lua_pushnumber(L, state->cb_func[cb]);
	return 1;
}


static static int callback_show(lua_State * L)
{
	State* state = check_state(L, 1);
	int i;
	luaL_checkstack(L, 3, "out of stack space");
	lua_newtable(L);
	for (i = 1; cb_names[i]; i++)
	{
		lua_pushstring(L, cb_names[i]);
		lua_pushboolean(L, state->cb_func[i] != 0);
		lua_rawset(L, -3);
	}
	return 1;
}


static int get_callback(lua_State * L, State* state, int i)
{
	luaL_checkstack(L, 2, "out of stack space");
	lua_rawgeti(L, LUA_REGISTRYINDEX, state->cb_func[i]);
	if (lua_isfunction(L, -1))
	{
		return 1 /* true */;
	}
	else
	{
		return 0 /* false */;
	}
}


/* Class Management */

static int testcb_new (lua_State *L)
{
	State* state = malloc(sizeof(State));
	memset(state, 0, sizeof(State));
	state->id = ++last_id;
	push_state(L, state);
	return 1;
}


static int testcb_destroy (lua_State *L)
{
	State* state = (State*)lua_unboxpointer(L, 1);
	if (state)
	{
		free(state);
		state = NULL;
	}
	return 0;
}


static int testcb_test (lua_State *L)
{
	State* state = check_state(L, 1);
	if (state->cb_func[cb_test] != 0)
		lua_rawgeti(L, LUA_REGISTRYINDEX, state->cb_func[cb_test]);
	printf("%d elements in the stack before callback\n", lua_gettop(L));
	lua_pushnumber(L, state->id);
	lua_call(L, 1 /* nargs */, 1 /* nresults */);
	printf("%d elements in the stack after callback\n\n", lua_gettop(L));
	lua_pushnumber(L, state->id);
	return 1;
}


static const luaL_reg meta_methods[] =
{
	{"__gc", testcb_destroy },
	{0,0}
};

static const luaL_reg main_methods[] =
{
	{"new", testcb_new},
	{"do_test", testcb_test},

	{"set_callback", callback_register},
	{"get_callbacks", callback_show},

	{0,0}
};

int luaopen_testcb_mod (lua_State *L)
{
	int metatable, methods;
	lua_newtable(L);
	methods   = lua_gettop(L); /* methods table */
	lua_newtable(L);
	metatable = lua_gettop(L); /* metatable */
	/* add index event to metatable */
	lua_pushliteral(L, "__index");
	lua_pushvalue(L, methods);
	lua_settable(L, metatable); /* metatable.__index = methods */
	/* hide metatable */
	lua_pushliteral(L, "__metatable");
	lua_pushvalue(L, methods);
	lua_settable(L, metatable); /* metatable.__metatable = methods */
	/* fill metatable */
	luaL_openlib(L, 0, meta_methods, 0);
	/* fill methods table */
	luaL_openlib(L, 0, main_methods, 1);
	return 1;
}

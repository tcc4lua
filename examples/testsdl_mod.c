/*
 * Copyright (C) 2009  Miriam Ruiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Adapted from: http://www.friedspace.com/cprogramming/sdlbasic.php
 */

#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <SDL/SDL.h>

#pragma luatcc use_library(SDL)

#define WIDTH 640
#define HEIGHT 480
#define BPP 4
#define DEPTH 32

typedef struct S_State
{
	SDL_Surface *screen;
	int h;
} State;

static void SetPixel(SDL_Surface *screen, int x, int y, Uint8 r, Uint8 g, Uint8 b)
{
	Uint32 *pixmem32;
	Uint32 colour;

	colour = SDL_MapRGB( screen->format, r, g, b );

	pixmem32 = (Uint32*) screen->pixels  + y + x;
	*pixmem32 = colour;
}


static void DrawScreen(SDL_Surface* screen, int h)
{
	int x, y, ytimesw;

	if(SDL_MUSTLOCK(screen))
	{
		if(SDL_LockSurface(screen) < 0) return;
	}

	for(y = 0; y < screen->h; y++ )
	{
		ytimesw = y*screen->pitch/BPP;
		for( x = 0; x < screen->w; x++ )
		{
			SetPixel(screen, x, ytimesw, (x*x)/256+3*y+h, (y*y)/256+x+h, h);
		}
	}

	if(SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);

	SDL_Flip(screen);
}


static int sdltest_init(lua_State* L)
{
	State* state;
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(State**)lua_touserdata(L, 1);

	printf("Init Method called\n");

	if( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		return luaL_error(L, "Couldn't initialize SDL: %s\n", SDL_GetError());
	}

	if (!(state->screen = SDL_SetVideoMode(WIDTH, HEIGHT, DEPTH, SDL_HWSURFACE)))
	{
		SDL_Quit();
		return luaL_error(L, "Couldn't set video mode: %s\n", SDL_GetError());
	}

	return 0;
};

static int sdltest_destroy(lua_State* L)
{
	State* state;
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(State**)lua_touserdata(L, 1);

	printf("Destroy Method called\n");
	SDL_Quit();
	return 0;
};

static int sdltest_events(lua_State* L)
{
	State* state;
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(State**)lua_touserdata(L, 1);

	printf("Events Method called\n");

	SDL_Event event;
	int keypress = 0;

	DrawScreen(state->screen,state->h++);
	while(SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				keypress = 1;
				break;
			case SDL_KEYDOWN:
				keypress = 1;
				break;
		}
	}

	lua_pushboolean (L, keypress);
	return 1;
};

static int sdltest_gc(lua_State* L)
{
	State** pstate;
	luaL_checktype(L, 1, LUA_TUSERDATA);
	pstate = (State**)lua_touserdata(L, 1);

	printf("Garbage Collector called\n");

	if (pstate && *pstate)
	{
		free(*pstate);
		*pstate = NULL;
	}

	return 0;
};

static const struct luaL_Reg sdltest_functions[] =
{
	{"init", sdltest_init},
	{"destroy", sdltest_destroy},
	{"events", sdltest_events},
	{NULL, NULL}
};

int luaopen_testsdl_mod(lua_State *L)
{
	State* state = malloc(sizeof(State));
	memset(state, 0, sizeof(State));
	state->h = 0;
	State** pstate = (State**)lua_newuserdata(L, sizeof(State*));
	*pstate = state;

	lua_newtable(L); /* Metatable */
	lua_newtable(L); /* __index */
	luaL_register(L, 0, sdltest_functions);
	lua_setfield(L, -2, "__index");
	lua_pushcfunction(L, sdltest_gc);
	lua_setfield(L, -2, "__gc");
	lua_setmetatable(L, -2);
	return 1;
}

/*
 * Copyright (C) 2009  Miriam Ruiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Adapted from: http://lua-users.org/wiki/BindingWithMetatableAndClosures
 */

#include <stdio.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <gd.h>

#pragma luatcc use_library(gd)

#define IMAGE "Image"

static gdImagePtr checkimage (lua_State *L, int index)
{
	luaL_checktype(L, index, LUA_TUSERDATA);
	lua_getmetatable(L, index);
	if( ! lua_equal(L, lua_upvalueindex(1), -1) )
								 /* die */
		luaL_typerror(L, index, IMAGE);
	lua_pop(L, 1);
	return (gdImagePtr)lua_unboxpointer(L, index);
}


static gdImagePtr pushimage (lua_State *L, gdImagePtr im)
{
	lua_boxpointer(L, im);
	lua_pushvalue(L, lua_upvalueindex(1));
	lua_setmetatable(L, -2);
	return im;
}


static int image_new (lua_State *L)
{
	int x = luaL_checkint(L, 1);
	int y = luaL_checkint(L, 2);
	pushimage(L, gdImageCreate(x, y));
	return 1;
}


static int image_destroy (lua_State *L)
{
	gdImagePtr im = (gdImagePtr)lua_unboxpointer(L, 1);
	if (im) gdImageDestroy(im);
	return 0;
}


static int image_color_allocate (lua_State *L)
{
	gdImagePtr im = checkimage(L, 1);
	int r = luaL_checkint(L, 2);
	int g = luaL_checkint(L, 3);
	int b = luaL_checkint(L, 4);
	lua_pushnumber(L, gdImageColorAllocate(im, r, g, b));
	return 1;
}


static int image_line (lua_State *L)
{
	gdImagePtr im = checkimage(L, 1);
	int x1     = luaL_checkint(L, 2);
	int y1     = luaL_checkint(L, 3);
	int x2     = luaL_checkint(L, 4);
	int y2     = luaL_checkint(L, 5);
	int colour = luaL_checkint(L, 6);
	gdImageLine(im, x1, y1, x2, y2, colour);
	return 0;
}


static int image_png (lua_State *L)
{
	/* Output the image to the disk file in PNG format. */
	gdImagePtr im    = checkimage(L, 1);
	const char *name = luaL_checkstring(L, 2);
	FILE *pngout     = fopen( name, "wb");
	gdImagePng(im, pngout);
	fclose(pngout);
	return 0;
}


static const luaL_reg meta_methods[] =
{
	{"__gc", image_destroy },
	{0,0}
};

static const luaL_reg image_methods[] =
{
	{"new",           image_new},
	{"colorallocate", image_color_allocate},
	{"line",          image_line},
	{"PNG",           image_png},
	{0,0}
};

#define newtable(L) (lua_newtable(L), lua_gettop(L))

int luaopen_image2_mod (lua_State *L)
{
	int metatable, methods;

	lua_pushliteral(L, IMAGE); /* name of Image table */
	methods   = newtable(L); /* Image methods table */
	metatable = newtable(L); /* Image metatable */
	/* add index event to metatable */
	lua_pushliteral(L, "__index");
	lua_pushvalue(L, methods);
	lua_settable(L, metatable); /* metatable.__index = methods */
	/* hide metatable */
	lua_pushliteral(L, "__metatable");
	lua_pushvalue(L, methods);
	lua_settable(L, metatable); /* metatable.__metatable = methods */
	/* fill metatable */
	luaL_openlib(L, 0, meta_methods,  0);
	/* fill Image methods table */
	luaL_openlib(L, 0, image_methods, 1);
	/* add Image to globals */
	lua_settable(L, LUA_GLOBALSINDEX);
	return 0;
}

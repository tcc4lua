/*
 * Copyright (C) 2009  Miriam Ruiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Adapted from: http://www.gettingclever.com/2008/08/integrating-sdl-with-lua.html
 */

#include <lua.h>
#include <lauxlib.h>
#include <SDL/SDL.h>
#include <GL/gl.h>

#pragma luatcc use_library(SDL)
#pragma luatcc use_library(GL)

#define WIDTH 400
#define HEIGHT 400
#define DEPTH 32

static int gltest_redraw(lua_State *L)
{
	float theta = lua_tonumber(L, 1);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,0.0f);
	glRotatef(theta, 0.0f, 0.0f, 1.0f);
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.87f, -0.5f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(-0.87f, -0.5f);
	glEnd();

	SDL_GL_SwapBuffers();
}


static int gltest_init(lua_State* L)
{
	if( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		return luaL_error(L, "Couldn't initialize SDL: %s\n", SDL_GetError());
	}

	SDL_Surface *screen = SDL_SetVideoMode(WIDTH, HEIGHT, DEPTH, SDL_DOUBLEBUF | SDL_HWSURFACE | SDL_OPENGL);

	glViewport(0, 0, 400, 400);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glMatrixMode(GL_PROJECTION);
	glMatrixMode(GL_MODELVIEW);

	lua_pushstring(L, "Hello World!");
	return 1;
};

static int gltest_destroy(lua_State* L)
{
	SDL_Quit();
	return 0;
};

static const struct luaL_Reg gltest_functions[] =
{
	{"init", gltest_init},
	{"destroy", gltest_destroy},
	{"redraw", gltest_redraw},
	{NULL, NULL}
};

int luaopen_testgl_mod(lua_State *L)
{
	lua_createtable(L, 2, 1);
	lua_pushvalue(L, LUA_ENVIRONINDEX);
	luaL_register(L, 0, gltest_functions);
	return 1;
}

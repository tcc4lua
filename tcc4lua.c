/*
 * Copyright (c) 2006-2007 Jérôme Vuarand
 * Copyright (c) 2009 Miriam Ruiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <lua.h>
#include <lauxlib.h>
#include <libtcc.h>
#include <stdlib.h>
#include <string.h>

#define LUATCC_DEBUG_LEVEL 2

#if LUATCC_DEBUG_LEVEL > 2
#define MSG_INFO printf
#else
#define MSG_INFO(...)
#endif

#if LUATCC_DEBUG_LEVEL > 1
#define MSG_WARNING printf
#else
#define MSG_WARNING(...)
#endif

#define MSG_ERROR printf

typedef struct S_LuaTCCState
{
	TCCState* tcc;
	void *mem;
#if LUATCC_DEBUG_LEVEL > 3
	unsigned int id;
#endif
} LuaTCCState;

#if LUATCC_DEBUG_LEVEL > 3
static unsigned int last_id = 0;
#endif

static int busy_tcc = 0;

/* function context:compile(source [, chunkname]) return true end */
static int lua__tcc__compile(lua_State* L)
{
	LuaTCCState* state;
	TCCState* tcc;
	const char* chunkname = NULL;
	const char* code;
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(LuaTCCState**)lua_touserdata(L, 1);
	tcc = state->tcc;
	
	code = luaL_checkstring(L, 2);

	if (lua_isstring(L, 3))
		chunkname = lua_tostring(L, 3);
	
	/* compile */
	if (tcc_compile_named_string(tcc, code, chunkname))
	{
		return luaL_error(L, "unknown compilation error");
	}
	
	lua_pushboolean(L, 1);
	return 1;
}

/* function context:add_library(libraryname) return true end */
static int lua__tcc__add_library(lua_State* L)
{
	LuaTCCState* state;
	TCCState* tcc;
	const char* libname = NULL;
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(LuaTCCState**)lua_touserdata(L, 1);
	tcc = state->tcc;
	
	libname = luaL_checkstring(L, 2);
	
	/* add libs */
	if (tcc_add_library(tcc, libname))
		return luaL_error(L, "can't load library %s", libname);
	
	lua_pushboolean(L, 1);
	return 1;
}

/* function context:relocate() return true end */
static int lua__tcc__relocate(lua_State* L)
{
	LuaTCCState* state;
	TCCState* tcc;
	int size;
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(LuaTCCState**)lua_touserdata(L, 1);
	tcc = state->tcc;
	
	if (state->mem != NULL)
	{
		return luaL_error(L, "cannot relocate twice the same context");
	}
	
	/* get needed size of the code */
	size = tcc_relocate(tcc, NULL);
	if (size == -1)
	{
		return luaL_error(L, "unknown relocation (link) error");
	}
	
	state->mem = malloc(size);
	tcc_relocate(tcc, state->mem);
	
	busy_tcc = 0;
	
	lua_pushboolean(L, 1);
	return 1;
}

/* function context:get_symbol(symbolname) return symbol end */
static int lua__tcc__get_symbol(lua_State* L)
{
	LuaTCCState* state;
	TCCState* tcc;
	const char* funcname = NULL;
	int (*func)(int);
	lua_CFunction f;
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(LuaTCCState**)lua_touserdata(L, 1);
	tcc = state->tcc;
	
	funcname = luaL_checkstring(L, 2);
	
	func = tcc_get_symbol(tcc, funcname);
	if (!func)
		return luaL_error(L, "can't get symbol %s", funcname);

	f = (lua_CFunction)func;
	
	lua_pushcclosure(L, f, 0);
	
	return 1;
}

/* function context:define_symbol(symbolname) return true end */
static int lua__tcc__define_symbol(lua_State* L)
{
	LuaTCCState* state;
	TCCState* tcc;
	const char* defname = NULL;
	const char* defvalue = NULL;
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(LuaTCCState**)lua_touserdata(L, 1);
	tcc = state->tcc;
	
	defname = luaL_checkstring(L, 2);
	defvalue = luaL_checkstring(L, 3);
	
	tcc_define_symbol(tcc, defname, defvalue);
	
	lua_pushboolean(L, 1);
	return 1;
}

/* function context:undefine_symbol(symbolname) return true end */
static int lua__tcc__undefine_symbol(lua_State* L)
{
	LuaTCCState* state;
	TCCState* tcc;
	const char* defname = NULL;
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(LuaTCCState**)lua_touserdata(L, 1);
	tcc = state->tcc;
	
	defname = luaL_checkstring(L, 2);
	
	tcc_undefine_symbol(tcc, defname);
	
	lua_pushboolean(L, 1);
	return 1;
}

/* function context:add_library_path(path) end */
static int lua__tcc__add_library_path(lua_State *L)
{
	LuaTCCState* state;
	TCCState* tcc;
	const char* path;
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(LuaTCCState**)lua_touserdata(L, 1);
	tcc = state->tcc;
	
	path = luaL_checkstring (L, 2);
	
	tcc_add_library_path(tcc, path);
	
	return 0;
}

/* function context:add_include_path(path) end */
static int lua__tcc__add_include_path(lua_State *L)
{
	LuaTCCState* state;
	TCCState* tcc;
	const char* path;
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	state = *(LuaTCCState**)lua_touserdata(L, 1);
	tcc = state->tcc;
	
	path = luaL_checkstring (L, 2);
	
	tcc_add_include_path(tcc, path);
	
	return 0;
}

static int lua__tcc___gc(lua_State* L)
{
	LuaTCCState** pstate;
	MSG_INFO("TCC: Garbage Collector\n");
	
	luaL_checktype(L, 1, LUA_TUSERDATA);
	pstate = (LuaTCCState**)lua_touserdata(L, 1);
	
	if (pstate && *pstate)
	{
#if LUATCC_DEBUG_LEVEL > 3
		MSG_INFO("TCC: - Deleting state #%d\n", (*pstate)->id);
#endif
		if ((*pstate)->tcc != NULL)
		{
			tcc_delete((*pstate)->tcc);
			(*pstate)->tcc = NULL;
		}
		if ((*pstate)->mem != NULL)
		{
			free((*pstate)->mem);
			(*pstate)->mem = NULL;
		}
		free(*pstate);
	}
	
	return 0;
}

static const struct luaL_reg tcc_methods[] = {
	{"compile", lua__tcc__compile},
	{"add_library", lua__tcc__add_library},
	{"relocate", lua__tcc__relocate},
	{"get_symbol", lua__tcc__get_symbol},
	{"define_symbol", lua__tcc__define_symbol},
	{"undefine_symbol", lua__tcc__undefine_symbol},
	{"add_library_path", lua__tcc__add_library_path},
	{"add_include_path", lua__tcc__add_include_path},
	{NULL, NULL}
};

void luatcc__error_func(void* opaque, const char* msg)
{
	luaL_error((lua_State*)opaque, "%s", msg);
}

static int lua__new(lua_State* L)
{
	MSG_INFO("TCC: New TCC Context\n");
	
	if (busy_tcc)
		return luaL_error(L, "state compilation needs to be finished before creating new ones");

	LuaTCCState* state = malloc(sizeof(LuaTCCState));
	memset(state, 0, sizeof(LuaTCCState));
	state->mem = NULL;
	
	state->tcc = tcc_new();
	if (!state->tcc)
		return luaL_error(L, "can't create tcc state");
	
#if LUATCC_DEBUG_LEVEL > 3
	state->id = ++last_id;
	MSG_INFO("TCC: + Creating state #%d\n", state->id);
#endif
	
	busy_tcc = 1;
	
	tcc_set_output_type(state->tcc, TCC_OUTPUT_MEMORY);
	tcc_set_error_func(state->tcc, (void*)L, luatcc__error_func);
	
	LuaTCCState** pstate = (LuaTCCState**)lua_newuserdata(L, sizeof(LuaTCCState*));
	*pstate = state;
	lua_newtable(L); /* Metatable */
	lua_newtable(L); /* __index */
	luaL_register(L, 0, tcc_methods);
	lua_setfield(L, -2, "__index");
	lua_pushcfunction(L, lua__tcc___gc);
	lua_setfield(L, -2, "__gc");
	lua_setmetatable(L, -2);
	
	return 1;
}

static const struct luaL_reg module_functions[] = {
	{"new", lua__new},
	{NULL, NULL}
};

MODULE_API int luaopen_module(lua_State *L)
{
	lua_getglobal(L, "module");
	lua_pushvalue(L, 1);
	lua_call(L, 1, 0);
	
	lua_pushvalue(L, LUA_ENVIRONINDEX);
	luaL_register(L, 0, module_functions);
	
	return 0;
}
